import React, { Component } from "react";
import {
  giamSoLuongAction,
  tangSoLuongAction,
} from "./Redux/Action/numberAction";

import { connect } from "react-redux";

class Demo_Redux_Mini_Recode extends Component {
  render() {
    return (
      <div>
        <button
          className="btn btn-info text-white"
          onClick={() => {
            this.props.giamSoLuong(10);
          }}
        >
          Giảm Số Lượng
        </button>
        <span className="display-4 text-danger">{this.props.soLuong}</span>
        <button
          onClick={this.props.tangSoLuong}
          className="btn btn-success text-warning"
        >
          Tăng Số Lượng
        </button>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    soLuong: state.numberReducer.number,
  };
};

let mapDispatchToProps = (dispatch) => {
  return {
    tangSoLuong: (soLuong) => {
      dispatch(tangSoLuongAction(soLuong));
    },
    giamSoLuong: (soLuong) => {
      dispatch(giamSoLuongAction(soLuong));
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Demo_Redux_Mini_Recode);
// hahahaha
