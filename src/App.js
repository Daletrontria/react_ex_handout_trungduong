import logo from "./logo.svg";
import "./App.css";
import Ex_ReactJS_Buoi1 from "./Ex_ReactJS_Buoi1/Ex_ReactJS_Buoi1";
import Render_Glasses from "./Ex_ReactJS_Buoi2/Render_Glasses";
import Demo_Redux_Mini_Recode from "./Demo_Redux_Mini_Recode/Demo_Redux_Mini_Recode";
function App() {
  return (
    <div className="App">
      {/* <Ex_ReactJS_Buoi1 /> */}
      {/* <Render_Glasses /> */}
      <Demo_Redux_Mini_Recode />
    </div>
  );
}

export default App;
