import React, { Component } from "react";
import { renderIntoDocument } from "react-dom/test-utils";
import { data } from "./Data_Glasses";
import style from "./Glasses.module.css";
export default class Render_Glasses extends Component {
  state = {
    glasses: data,
    glassDetail: data[0],
  };
  //   Hàm thay kính
  handleChangeGlass = (data) => {
    console.log("data", data);
    this.setState({
      glassDetail: data,
    });
  };
  renderList = () => {
    return this.state.glasses.map((item, index) => {
      console.log("item", item);
      return (
        <img
          key={index}
          onClick={() => {
            this.handleChangeGlass(item);
          }}
          // width:`${100/this.state.glasses.lenght}%`
          style={{ width: 200, padding: "0px 5px" }}
          src={item.url}
          alt=""
        ></img>
      );
    });
  };
  renderGlass = () => {
    return (
      <div className={style.tryOn}>
        <div className={style.KM}>
          <img
            style={{
              top: "100px",
              width: "55%",
            }}
            src={this.state.glassDetail.url}
            alt=""
          ></img>
        </div>

        <div className={style.Model}>
          <img
            src="./glassesImage/model.jpg"
            style={{
              width: "400px",
            }}
            alt=""
          ></img>
        </div>
      </div>
    );
  };

  render() {
    return (
      <div>
        {this.renderGlass()}
        {this.renderList()}
      </div>
    );
  }
}
