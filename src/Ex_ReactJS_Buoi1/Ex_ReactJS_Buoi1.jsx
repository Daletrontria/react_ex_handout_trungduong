import React, { Component } from "react";
import Banner from "./Banner";
import Item from "./Item";
import List from "./List";
import Header from "./Header";
export default class Ex_ReactJS_Buoi1 extends Component {
  render() {
    return (
      <div>
        <div>
          <Header />
          <Banner />

          <List />
        </div>
      </div>
    );
  }
}
